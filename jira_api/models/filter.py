import json


class Filter:
    def __init__(self,
                 id: int = None,
                 jql: str = None,
                 name: str = None,
                 description: str = None):
        self.id = id
        self.jql = jql
        self.name = name
        self.description = description

    def to_dict(self) -> dict:
        object_dict = {
            'id': self.id,
            'jql': self.jql,
            'name': self.name,
            'description': self.description
        }

        # remove `None` values from the dict
        object_dict = {k: v for k, v in object_dict.items() if v is not None}

        return object_dict

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(data: dict) -> 'Filter':
        return Filter(id=data.get('id'),
                      jql=data.get('jql'),
                      name=data.get('name'),
                      description=data.get('description'))
