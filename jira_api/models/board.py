import json


class Board:
    def __init__(self,
                 id: int = None,
                 name: str = None,
                 type: str = None,
                 filter_id: int = None):
        self.id = id
        self.name = name
        self.type = type
        self.filter_id = filter_id

    def to_dict(self) -> dict:
        object_dict = {
            'id': self.id,
            'type': self.type,
            'name': self.name,
            'filterId': self.filter_id
        }

        # remove `None` values from the dict
        object_dict = {k: v for k, v in object_dict.items() if v is not None}

        return object_dict

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(data: dict) -> 'Board':
        return Board(id=data.get('id'),
                     name=data.get('name'),
                     type=data.get('type'),
                     filter_id=data.get('filterId'))
