# Filter and get objects wich person/group,
# and where the parameter(cn/displayName/sAMAccountName/mailNickname) starting with the term
AD_USERS_N_GROUP_SEARCH_TERM = '(&(|(objectclass=person)(objectclass=group)) \
(|(cn={term}*)(displayName={term}*)(sAMAccountName={term}*)(mailNickname={term}*)))'