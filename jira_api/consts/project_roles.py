from enum import Enum


class ProjectRoles(Enum):
    developers = 10001
    administrators = 10002
