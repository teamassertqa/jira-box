from enum import Enum


class WorkingDays(Enum):
    day = dict(monday='true',
               tuesday='true',
               wednesday='true',
               thursday='true',
               friday='false',
               saturday='false',
               sunday='true')
