from enum import Enum


class AssigneeType(Enum):
    unassigned = 'UNASSIGNED'
    project_lead = 'PROJECT_LEAD'
