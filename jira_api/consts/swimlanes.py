from enum import Enum


class SwimLanes(Enum):
    none = 'none'
    epics = 'epic'
    stories = 'parentChild'
    projects = 'project'
    assignees = 'assignee'
