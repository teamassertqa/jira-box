class JiraError(Exception):
    def __init__(self, response: dict):
        if len(response['errorMessages']) > 0:
            message = response['errorMessages']
        elif len(response['errors']) > 0:
            message = response['errors']
        else:
            message = "Unknown exception"
        super().__init__(message)
