import json
from json import JSONDecodeError
from typing import Union, List

import requests
from atlassian.rest_client import AtlassianRestAPI

import config
from jira_api.models.project import Project
from jira_api.errors.jira_error import JiraError

from jira_api.consts.routes import PROJECT_ROUTE, PROJECT_ADD_ROLES_ROUTE, PROJECT_VALIDATE_ROUTE, \
    PROJECT_CATEGORIES_ROUTE, GREEN_HOPPER_ROUTE


class ProjectHandler:
    def __init__(self,
                 jira_api_client: AtlassianRestAPI):
        self.jira_conn = jira_api_client
        self.config = config.get_configuration()

    def read(self, id_or_key: Union[int, str]) -> Project:
        response = self.jira_conn.get(path=f"{PROJECT_ROUTE}/{id_or_key}")
        if 'errorMessages' in response or 'errors' in response:
            raise JiraError(response)
        return Project.from_jira(response)

    def delete(self, id: int):
        response = self.jira_conn.delete(path=f"{PROJECT_ROUTE}/{id}")
        if response is not None and ('errorMessages' in response or 'errors' in response):
            raise JiraError(response)

    def create(self, project: Project) -> Project:
        project.workflow_scheme_id = self.config.WORKFLOW_SCHEME_ID

        response = self.jira_conn.post(path=PROJECT_ROUTE, data=project.to_jira())

        if 'errorMessages' in response or 'errors' in response:
            raise JiraError(response)

        response_project = Project.from_jira(data=response)
        project.jira_id = response_project.jira_id
        return project

    def update(self, id: int, project: Project) -> Project:
        data = project.to_jira()
        response = self.jira_conn.put(path=f"{PROJECT_ROUTE}/{id}", data=data)
        if 'errorMessages' in response or 'errors' in response:
            raise JiraError(response)
        return Project.from_jira(response)

    def is_exists(self, key: str) -> bool:
        response = self.jira_conn.get(path=f"{PROJECT_VALIDATE_ROUTE}?key={key}")

        if not isinstance(response, dict):
            raise JiraError({"errorMessages": "Jira is unavailable right now"})

        if 'projectKey' in response['errors']:
            return True
        return False

    def validate_key(self, key: str) -> dict:
        response = self.jira_conn.get(path=f"{PROJECT_VALIDATE_ROUTE}?key={key}")
        if 'projectKey' in response['errors']:
            return {
                'validated': False,
                'error': response['errors']['projectKey']
            }
        return {
            'validated': True
        }

    def get_all_projects(self) -> List[Project]:
        response = self.jira_conn.get(path=PROJECT_ROUTE)
        return list({v['id']: Project.from_jira(v) for v in response}.values())

    def get_projects_categories(self) -> dict:
        return self.jira_conn.get(path=PROJECT_CATEGORIES_ROUTE)

    def add_roles(self, project_key, group_id: int, users: list = [], groups: list = []):
        payload = {'groups': groups, 'users': users}
        self.jira_conn.post(path=f"{PROJECT_ADD_ROLES_ROUTE}/{project_key}/{group_id}",
                            data=payload)

    def setup_columns(self, board_id: int):
        s = requests.Session()
        payload = {
            "currentStatisticsField": {"id": "none_"},
            "rapidViewId": board_id,
            "mappedColumns": [
                {"mappedStatuses": [{"id": "10900"}], "id": None, "name": "To Do", "isKanPlanColumn": False},
                {"mappedStatuses": [{"id": "3"}], "id": None, "name": "In Progress", "isKanPlanColumn": False},
                {"mappedStatuses": [{"id": "11101"}], "id": None, "name": "CR", "isKanPlanColumn": False},
                {"mappedStatuses": [{"id": "12200"}], "id": None, "name": "Stage", "isKanPlanColumn": False},
                {"mappedStatuses": [{"id": "6"}], "id": None, "name": "Done", "isKanPlanColumn": False}
            ]}

        s.put(url=f'{self.config.JIRA_URL}{GREEN_HOPPER_ROUTE}/rapidviewconfig/columns',
              headers={"Content-Type": "application/json"},
              data=json.dumps(payload),
              auth=(self.config.ATLASSIAN_USERNAME, self.config.ATLASSIAN_PASSWORD),
              verify=False)
