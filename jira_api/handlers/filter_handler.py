from atlassian.rest_client import AtlassianRestAPI

from jira_api.models.filter import Filter

from jira_api.consts.routes import FILTER_ROUTE


class FilterHandler:
    def __init__(self,
                 jira_api_client: AtlassianRestAPI):
        self.jira_conn = jira_api_client

    def read(self, id: int) -> Filter:
        response = self.jira_conn.get(path=f"{FILTER_ROUTE}/{id}")
        return Filter.from_json(response)

    def delete(self, id: int) -> Filter:
        response = self.jira_conn.delete(path=f"{FILTER_ROUTE}/{id}")
        return Filter.from_json(response)

    def create(self, filter: Filter) -> Filter:
        response = self.jira_conn.post(path=FILTER_ROUTE, data=filter.to_dict())
        return Filter.from_json(response)

    def update(self, id: int, filter: Filter) -> Filter:
        response = self.jira_conn.put(path=f"{FILTER_ROUTE}/{id}", data=filter.to_dict())
        return Filter.from_json(response)
