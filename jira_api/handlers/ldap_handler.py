import random
from typing import List, Optional

from retry import retry
from ldap3 import Server, Connection, Entry
from ldap3.core.exceptions import LDAPException

from jira_api.consts.ad_users_n_groups_search_term import AD_USERS_N_GROUP_SEARCH_TERM
from resources import configuration


class LdapException(Exception):
    pass


class LDAPHandler:
    def __get_random_server(self):
        random_ldap_server = configuration.AD_SERVER_LIST[random.randint(0, len(configuration.AD_SERVER_LIST) - 1)]
        server = Server(random_ldap_server)
        return server

    # make search in ldap based on parameters
    @retry(LdapException, tries=5)
    def __search_ldap(self, search_parameters: dict) -> List:
        try:
            server = self.__get_random_server()
            with Connection(server,
                            user=configuration.LDAP_USERNAME,
                            password=configuration.LDAP_PASSWORD) as conn:
                conn.search(**search_parameters)
                if len(conn.entries) != 0:
                    return conn.entries
                return []
        except LDAPException as e:
            raise LdapException('An Error occurred when trying to search the AD using ldap protocol') from e

    @staticmethod
    def __get_display_name_from_entry(entry: Entry) -> Optional[str]:
        # The ldap entry might not have a displayName so its existence needs to be checked to avoid exception
        if hasattr(entry, 'displayName'):
            if entry.displayName.value:
                return entry.displayName.value
        if hasattr(entry, 'cn'):
            return entry.cn.value
        return None

    def search_users(self, term: str, search_base: str = 'zzsewarch_basezz', recursive: bool = False) -> List[dict]:
        result = []

        search_parameters = {'search_base': search_base,
                             'search_filter': AD_USERS_N_GROUP_SEARCH_TERM.format(term=term),
                             'attributes': ['cn', 'displayName', 'sAMAccountName', 'objectclass', 'member'],
                             'paged_size': 1}
        ldap_entities = self.__search_ldap(search_parameters)

        results = [{'username': entity.sAMAccountName.value,
                    'displayName': LDAPHandler.__get_display_name_from_entry(entity),
                    'isGroup': 'group' in entity.objectclass.value,
                    'member': entity.member if hasattr(entity, 'member') else None}
                   for entity in ldap_entities]

        if recursive:
            for res in results:
                if res.get('isGroup'):
                    for member in res.get('member'):
                        result += self.search_users('', member, recursive)
                else:
                    result += results
        else:
            result = results

        return result
