from typing import List

from atlassian.rest_client import AtlassianRestAPI

from jira_api.consts.swimlanes import SwimLanes
from jira_api.consts.working_days import WorkingDays
from jira_api.models.board import Board

from jira_api.consts.routes import BOARD_ROUTE, QUICK_FILTER_ROUTE, WORKING_DAYS_ROUTE, \
    SWIMLINE_ROUTE, BOARD_CONFIGURATION


class BoardHandler:
    def __init__(self,
                 jira_api_client: AtlassianRestAPI):
        self.jira_conn = jira_api_client

    def read(self, id: int) -> Board:
        response = self.jira_conn.get(path=f"{BOARD_ROUTE}/{id}")
        return Board.from_json(response)

    def delete(self, id: int) -> Board:
        response = self.jira_conn.delete(path=f"{BOARD_ROUTE}/{id}")
        return Board.from_json(response)

    def create(self, board: Board) -> Board:
        response = self.jira_conn.post(path=BOARD_ROUTE, data=board.to_dict())
        return Board.from_json(response)

    def update(self, id: int, board: Board) -> Board:
        response = self.jira_conn.put(path=f"{BOARD_ROUTE}/{id}", data=board.to_dict())
        return Board.from_json(response)

    def add_quick_filter(self, id: int, display_name: str, username: str):
        body = {"name": display_name, "query": "assignee = " + username}
        self.jira_conn.post(f'{QUICK_FILTER_ROUTE}/' + str(id), body)

    def remove_quick_filter(self, board_id: int, filter_id: int):
        self.jira_conn.delete(f'{QUICK_FILTER_ROUTE}/{board_id}/{filter_id}')

    def get_quick_filters(self, board_id: int) -> List[dict]:
        response = self.jira_conn.get(f'{BOARD_CONFIGURATION}?rapidViewId={board_id}')
        return response.get('quickFilterConfig').get('quickFilters')

    def get_project_boards(self, project_key_or_id: str) -> List[Board]:
        response = self.jira_conn.get(path=f'{BOARD_ROUTE}?projectKeyOrId={project_key_or_id}')
        if 'values' in response:
            return list({v['id']: Board.from_json(v) for v in response['values']}.values())
        return []

    def get_board_configuration(self, board_id: int) -> List[dict]:
        return self.jira_conn.get(path=f'{BOARD_ROUTE}/{board_id}/configuration')

    def set_swimlane_strategy(self, id: int, strategy: SwimLanes):
        data = {'id': id, 'swimlaneStrategyId': strategy.value}
        self.jira_conn.put(path=f'{SWIMLINE_ROUTE}', data=data)

    def set_working_days(self, id: int, working_days: WorkingDays):
        working_days = {**working_days.value, **{"rapidViews": id}}
        self.jira_conn.put(path=f'{WORKING_DAYS_ROUTE}', data=working_days)
