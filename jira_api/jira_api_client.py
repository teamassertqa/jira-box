import json
from json import JSONDecodeError

from atlassian.rest_client import AtlassianRestAPI

from jira_api.handlers.board_handler import BoardHandler
from jira_api.handlers.filter_handler import FilterHandler
from jira_api.handlers.project_handler import ProjectHandler


class JiraAPIClient:
    def __init__(self,
                 url: str,
                 **kwargs):
        self._atlassian = AtlassianRestAPI(url=url, verify_ssl=False, timeout=240, **kwargs)

        self.project = ProjectHandler(self._atlassian)
        self.board = BoardHandler(self._atlassian)
        self.filter = FilterHandler(self._atlassian)

    def jira_status(self) -> dict:
        data = self._atlassian.get(path="/status")

        if not isinstance(data, dict):
            data = {"state": "UNKNOWN"}

        switch = {
            "RUNNING": {"health": "OK", "message": "Running normally"},
            "ERROR": {"health": "BAD", "message": "An error state"},
            "STARTING": {"health": "BAD", "message": "Application is starting"},
            "STOPPING": {"health": "BAD", "message": "Application is stopping"},
            "FIRST_RUN": {"health": "BAD", "message": "Not configured"},
            "UNKNOWN": {"health": "BAD", "message": "Unknown error"},
        }
        response = switch.get(data.get("state"))

        return response
