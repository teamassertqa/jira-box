import pytest

from tests.e2e.infra.box_api.box_rest_client import BoxAPIClient
from tests.e2e.utils.deployment_helper import DeploymentHelper
from tests.e2e.utils.deployment_sampler import DeploymentSampler


@pytest.fixture(scope="session")
def deployment_sampler(box_api: BoxAPIClient) -> DeploymentSampler:
    return DeploymentSampler(box_api)


@pytest.fixture(scope="session")
def deployment_helper(box_api: BoxAPIClient,
                      deployment_sampler: DeploymentSampler) -> DeploymentHelper:
    return DeploymentHelper(box_api, deployment_sampler)
