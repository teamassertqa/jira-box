import string

import pytest

from jira_api.consts.assignee_type import AssigneeType
from jira_api.consts.jira_project_type import ProjectTypeKey
from jira_api.jira_api_client import JiraAPIClient
from jira_api.models.project import Project
from tests.e2e.infra.db.db_client import DBConnection
from tests.e2e.utils.deployment_helper import DeploymentHelper
from tests.e2e.infra.box_api.box_rest_client import BoxAPIClient
from tests.e2e.tests.project import config
from tests.e2e.utils.utils import get_random_value, get_random_from_list, get_random_value_from_enum, \
    get_random_key_from_dict


@pytest.fixture(scope="session")
def jira_api() -> JiraAPIClient:
    return JiraAPIClient(url=config.JIRA_URL,
                         username=config.ATLASSIAN_USERNAME,
                         password=config.ATLASSIAN_PASSWORD)


@pytest.fixture(scope="session")
def box_api() -> BoxAPIClient:
    return BoxAPIClient(host=config.BOX_API)


@pytest.fixture(scope="session")
def box_db() -> DBConnection:
    return DBConnection(connection_string=config.AMERICA_SQLALCHEMY_URI)


def generate_project(jira_api: JiraAPIClient) -> Project:
    project = Project(name=get_random_value(4, string.ascii_letters),
                      key=None,
                      type="jira-box-4",
                      user=config.ATLASSIAN_USERNAME,
                      users=["zzuserzz", "zzuser2zz"],
                      jira_id=None,
                      type_key="software",
                      description=get_random_value(20, string.printable),
                      category_id=get_random_from_list([10100, 10101, 10401]),
                      america_uuid=None,
                      assignee_type=get_random_value_from_enum(AssigneeType),
                      project_template_key=get_random_key_from_dict(ProjectTypeKey))

    while True:
        project.key = get_random_value(7)
        if not jira_api.project.is_exists(project.key):
            break

    print(f"Created data: {project.__dict__}")

    return project


@pytest.fixture(scope="function")
def non_existing_project(jira_api: JiraAPIClient) -> Project:
    project = generate_project(jira_api)

    yield project

    try:
        if project.jira_id is not None:
            jira_api.project.delete(project.key)
    except:
        pass


@pytest.fixture(scope="function")
def existing_project(jira_api: JiraAPIClient,
                     deployment_helper: DeploymentHelper) -> Project:
    data = generate_project(jira_api)
    project = deployment_helper.create_project(data)

    yield project

    try:
        deployment_helper.delete_project(project)
    except:
        pass
