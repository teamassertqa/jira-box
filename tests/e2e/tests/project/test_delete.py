import pytest

from jira_api.errors.jira_error import JiraError
from jira_api.jira_api_client import JiraAPIClient
from jira_api.models.project import Project
from tests.e2e.infra.db.db_client import DBConnection
from tests.e2e.utils.deployment_helper import DeploymentHelper
from tests.e2e.utils.deployment_sampler import DeploymentStatusError


class TestDeleteProject:
    def test_delete_project(self,
                            box_db: DBConnection,
                            jira_api: JiraAPIClient,
                            existing_project: Project,
                            deployment_helper: DeploymentHelper):

        project = jira_api.project.read(existing_project.jira_id)

        boards = jira_api.board.get_project_boards(project.key)

        deployment_helper.delete_project(existing_project)

        with pytest.raises(JiraError):
            assert jira_api.project.read(existing_project)

        # assert jira_project_deleted.jira_id is None, "Project still exists"

        for board in boards:
            deleted_board = jira_api.board.read(board.id)
            assert deleted_board.id is None, "Board still exists"

        deployment = box_db.deployment.read(existing_project.america_uuid)

        assert deployment.is_deleted is True, "Project not marked as deleted"

    def test_delete_not_exist_project(self,
                                      jira_api: JiraAPIClient,
                                      box_db: DBConnection,
                                      existing_project: Project,
                                      deployment_helper: DeploymentHelper):
        jira_api.project.delete(existing_project.jira_id)

        with pytest.raises(DeploymentStatusError):
            deployment_helper.delete_project(existing_project)

        deployment = box_db.deployment.read(existing_project.america_uuid)

        assert deployment.is_deleted is True, "Project not marked as deleted"
