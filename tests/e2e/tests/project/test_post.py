import string

import pytest

from jira_api.jira_api_client import JiraAPIClient
from jira_api.models.project import Project
from tests.e2e.utils.deployment_helper import DeploymentHelper
from tests.e2e.utils.utils import get_random_value


class TestValid:
    # send all correct params
    # * responsible team = quick filters ???
    # * board exists
    # * has our settings?
    # * has our workflow?
    # send all correct params send when owner is unknown for jira(but exists in AD) ~ gonna fail!
    # send all correct params send used key will failed and return error

    # TODO: "project_template_key" not checked
    @pytest.mark.parametrize("parameter_name", ["name", "user", "category_id", "description", "assignee_type", "key"])
    def test_valid_post(self,
                        parameter_name: str,
                        jira_api: JiraAPIClient,
                        existing_project: Project):
        jira_project = jira_api.project.read(existing_project.jira_id)

        assert str(existing_project.__getattribute__(parameter_name)) == jira_project.__getattribute__(parameter_name)

    # TODO: AST-88

    def test_no_error_post_not_exist_responsible_team(self,
                                                      non_existing_project: Project,
                                                      deployment_helper: DeploymentHelper):
        non_existing_project.users = ["userWhichNotExist"]

        deployment_helper.create_project(non_existing_project)

    def test_error_post_exist_key(self,
                                  existing_project: Project,
                                  deployment_helper: DeploymentHelper):
        with pytest.raises(Exception):
            assert deployment_helper.create_project(existing_project)

    @pytest.mark.parametrize("parameter_name,parameter_value", [("name", get_random_value(num_chars=81, chars=string.ascii_letters + string.digits)),
                                                                ("key", get_random_value(num_chars=4, prefix="A$")),
                                                                ("key", get_random_value(num_chars=11)),
                                                                ("key", get_random_value(num_chars=1)),
                                                                ("key", get_random_value(num_chars=3, prefix="2")),
                                                                ("user", get_random_value(num_chars=10, chars=string.ascii_letters)),
                                                                ("assignee_type", get_random_value(num_chars=10, chars=string.ascii_letters)),
                                                                ("category_id", get_random_value(num_chars=3, chars=string.digits)),
                                                                ("project_template_key", get_random_value(num_chars=10, chars=string.ascii_letters))])
    def test_invalid_post(self,
                          parameter_name: str,
                          parameter_value: str,
                          non_existing_project: Project,
                          deployment_helper: DeploymentHelper):
        non_existing_project.__setattr__(parameter_name, parameter_value)

        with pytest.raises(Exception):
            assert deployment_helper.create_project(non_existing_project)
