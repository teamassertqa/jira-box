import string

import pytest

from jira_api.jira_api_client import JiraAPIClient
from jira_api.models.project import Project
from tests.e2e.infra.box_api.box_rest_client import BoxAPIClient
from tests.e2e.infra.db.db_client import DBConnection
from tests.e2e.utils.utils import get_random_value, get_random_from_list


class TestGetProject:
    def test_valid_get_project(self,
                               box_api: BoxAPIClient,
                               jira_api: JiraAPIClient,
                               existing_project: Project):
        jira_box_project = box_api.project.read(existing_project.america_uuid)

        jira_project = jira_api.project.read(jira_box_project.jira_id)

        assert jira_project == jira_box_project, "projects not equal"

    """
    The test do this steps:
    * creating project in jira from our box
    * removing the project directly from jira(external remove)
    * trying to read project from our box
    * asserting if the deployment marked as deleted without any exceptions(cz we deleted it externally)
    """
    def test_get_external_deleted_project(self,
                                          box_db: DBConnection,
                                          box_api: BoxAPIClient,
                                          jira_api: JiraAPIClient,
                                          existing_project: Project):
        jira_api.project.delete(existing_project.jira_id)
        box_api.project.read(existing_project.america_uuid)

        project_deployment = box_db.deployment.read(existing_project.america_uuid)

        assert project_deployment.is_deleted is True, "External deleted project not marked as deleted"

    @pytest.mark.parametrize("parameter_name,parameter_value", [("name", get_random_value(num_chars=7, chars=string.ascii_letters+string.digits)),
                                                                ("key", get_random_value(num_chars=7)),
                                                                ("description", get_random_value(num_chars=10, chars=string.printable)),
                                                                ("category_id", get_random_from_list([10100, 10101, 10401]))])
    def test_get_project_details_after_external_changes(self,
                                                        parameter_name: str,
                                                        parameter_value: str,
                                                        box_api: BoxAPIClient,
                                                        jira_api: JiraAPIClient,
                                                        existing_project: Project):
        box_project_data = box_api.project.read(existing_project.america_uuid)

        new_project = Project(name=box_project_data.name,
                              user=box_project_data.user,
                              jira_id=box_project_data.jira_id,
                              key=box_project_data.key,
                              type_key=box_project_data.type_key,
                              description=box_project_data.description,
                              assignee_type=box_project_data.assignee_type,
                              category_id=box_project_data.category_id,
                              project_template_key=box_project_data.project_template_key, )

        new_project.__setattr__(parameter_name, parameter_value)
        jira_api.project.update(box_project_data.jira_id, new_project)

        box_project_data_new = box_api.project.read(existing_project.america_uuid)

        assert str(parameter_value) == str(box_project_data_new.__getattribute__(parameter_name)), f"{parameter_name} not updated"
