import string

import pytest

from jira_api.consts.assignee_type import AssigneeType
from jira_api.consts.jira_project_type import ProjectTypeKey
from jira_api.jira_api_client import JiraAPIClient
from jira_api.models.project import Project
from tests.e2e.infra.db.db_client import DBConnection
from tests.e2e.utils.deployment_helper import DeploymentHelper
from tests.e2e.utils.deployment_sampler import DeploymentStatusError
from tests.e2e.utils.utils import get_random_value, get_random_from_list, get_random_key_from_dict, \
    get_random_value_from_enum


class TestUpdateProject:
    def test_update_not_exist_project(self,
                                      jira_api: JiraAPIClient,
                                      box_db: DBConnection,
                                      existing_project: Project,
                                      deployment_helper: DeploymentHelper):
        existing_project.description = "new testing description"

        jira_api.project.delete(existing_project.jira_id)

        with pytest.raises(Exception):
            assert deployment_helper.update_project(existing_project.america_uuid, existing_project)

        deployment = box_db.deployment.read(existing_project.america_uuid)

        assert deployment.is_deleted is True, "Project not marked as deleted"

    @pytest.mark.parametrize("parameter_name,parameter_value",
                             [("name", get_random_value(num_chars=4, chars=string.ascii_letters + string.digits)),
                              ("key", get_random_value(num_chars=7)),
                              ("description", get_random_value(num_chars=15, chars=string.printable)),
                              ("category_id", get_random_from_list([10100, 10101, 10401])),
                              # ("project_template_key", get_random_key_from_dict(ProjectTypeKey)),
                              ("assignee_type", get_random_value_from_enum(AssigneeType))])
    def test_valid_value(self,
                         parameter_name: str,
                         parameter_value: str,
                         jira_api: JiraAPIClient,
                         existing_project: Project,
                         deployment_helper: DeploymentHelper):
        existing_project.__setattr__(parameter_name, parameter_value)

        deployment_helper.update_project(existing_project.america_uuid, existing_project)

        new_project = jira_api.project.read(existing_project.jira_id)

        assert str(existing_project.__getattribute__(parameter_name)) == new_project.__getattribute__(parameter_name)

    def test_update_project_details_error_exist_key(self,
                                                    jira_api: JiraAPIClient,
                                                    existing_project: Project,
                                                    non_existing_project: Project,
                                                    deployment_helper: DeploymentHelper):
        project = deployment_helper.create_project(non_existing_project)
        old_key = project.key
        project.key = existing_project.key

        with pytest.raises(DeploymentStatusError):
            deployment_helper.update_project(project.america_uuid, project)

        jira_api.project.read(old_key)
