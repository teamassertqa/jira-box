from tests.e2e.infra.box_api.consts.http_method import HTTPMethod
from tests.e2e.infra.box_api.consts.routes import TRANSACTIONS_ROUTE
from tests.e2e.infra.models.america_transaction import AmericaTransaction


class TransactionsHandler:
    def __init__(self,
                 box_conn):
        self.box_conn = box_conn

    def read(self, transaction_id: str) -> AmericaTransaction:
        response = self.box_conn.request(method=HTTPMethod.get, url=f'{TRANSACTIONS_ROUTE}/{transaction_id}')
        return AmericaTransaction.from_json(response)
