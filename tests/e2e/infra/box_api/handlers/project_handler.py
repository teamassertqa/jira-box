from jira_api.models.project import Project
from tests.e2e.infra.box_api.consts.http_method import HTTPMethod
from tests.e2e.infra.box_api.consts.routes import PROJECT_ROUTE
from tests.e2e.infra.models.america_transaction import AmericaTransaction


class ProjectHandler:
    def __init__(self,
                 box_conn):
        self.box_conn = box_conn

    def create(self, project: Project) -> AmericaTransaction:
        response = self.box_conn.request(method=HTTPMethod.post, url=PROJECT_ROUTE, body=project.to_america())
        return AmericaTransaction.from_json(response)

    def read(self, deployment_id: str) -> Project:
        response = self.box_conn.request(method=HTTPMethod.get, url=f'{PROJECT_ROUTE}/{deployment_id}')
        return Project.from_america(response)

    def update(self, deployment_id: str, project: Project) -> AmericaTransaction:
        response = self.box_conn.request(method=HTTPMethod.put, url=f'{PROJECT_ROUTE}/{deployment_id}', body=project.to_america())
        return AmericaTransaction.from_json(response)

    def delete(self, deployment_id) -> AmericaTransaction:
        response = self.box_conn.request(method=HTTPMethod.delete, url=f'{PROJECT_ROUTE}/{deployment_id}')
        return AmericaTransaction.from_json(response)
