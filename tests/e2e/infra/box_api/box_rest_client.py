import requests
from retrying import retry

from tests.e2e.infra.box_api.consts.http_method import HTTPMethod
from tests.e2e.infra.box_api.handlers.project_handler import ProjectHandler

from tests.e2e.infra.box_api.handlers.transactions_handler import TransactionsHandler


# Connection Errors can cause because temporary connection failure or OpenShift
def retry_if_connection_error(exception: Exception):
    return isinstance(exception, requests.exceptions.ConnectionError) or \
           str(exception).__contains__("ProtocolError")


class BoxAPIClient:
    def __init__(self,
                 host: str):
        self.base_url = host
        self.project = ProjectHandler(self)
        self.transaction = TransactionsHandler(self)

    @retry(retry_on_exception=retry_if_connection_error, stop_max_attempt_number=2, wait_fixed=2000)
    def request(self,
                method: HTTPMethod,
                url: str,
                body: dict = None,
                headers: dict = None):
        if headers is None:
            headers = {}

        url = self.base_url + url

        response = requests.request(method=method.name, url=url, json=body, headers=headers)

        return response.json()
