from flask_america.db import AmericaDeployment
from sqlalchemy.orm import Session


class DeploymentHandler:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    def read(self, flow_id: str) -> AmericaDeployment:
        return self.db_session.query(AmericaDeployment) \
            .filter_by(id=flow_id) \
            .one()
