import sqlalchemy as db
from sqlalchemy.orm import sessionmaker

from tests.e2e.infra.db.handlers.deployment import DeploymentHandler


class DBConnection:
    def __init__(self,
                 connection_string: str):
        self.engine = db.create_engine(connection_string)
        Session = sessionmaker(bind=self.engine)
        self.db_session: Session = Session()

        self.deployment = DeploymentHandler(self.db_session)
