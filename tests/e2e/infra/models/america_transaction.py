import json


class AmericaTransaction:
    def __init__(self,
                 status: str,
                 transaction_id: str,
                 box_id: str = None):
        self.status = status
        self.box_id = box_id
        self.transaction_id = transaction_id

    def to_dict(self) -> dict:
        object_dict = {
            'status': self.status,
            'boxId': self.box_id,
            'transactionId': self.transaction_id,
        }

        # remove `None` values from the dict
        object_dict = {k: v for k, v in object_dict.items() if v is not None}

        return object_dict

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(data: dict) -> 'AmericaTransaction':
        return AmericaTransaction(status=data.get('status'),
                                  box_id=data.get('boxId'),
                                  transaction_id=data.get('transactionId'))
