from retrying import retry

from tests.e2e.infra.box_api.box_rest_client import BoxAPIClient
from tests.e2e.infra.models.america_transaction import AmericaTransaction


def retry_if_deployment_not_valid_error(exception: Exception) -> bool:
    return isinstance(exception, DeploymentNotValidError)


NUMBER_OF_RETRIES = 10
MILLISECONDS_BETWEEN_RETRIES = 20000


class DeploymentSampler:
    def __init__(self,
                 box_api: BoxAPIClient):
        self.box_api = box_api

    @retry(retry_on_exception=retry_if_deployment_not_valid_error, stop_max_attempt_number=NUMBER_OF_RETRIES,
           wait_fixed=MILLISECONDS_BETWEEN_RETRIES)
    def validate_deployment_status(self,
                                   transaction_id: str,
                                   expected_status: str) -> AmericaTransaction:
        transaction_response = self.box_api.transaction.read(transaction_id)

        if expected_status == transaction_response.status:
            return transaction_response

        if expected_status != "FAILED" and transaction_response.status == "FAILED":
            raise DeploymentStatusError(
                f"ERROR: The deployment {transaction_id} status is FAILED, instead of status {expected_status}")
        raise DeploymentNotValidError(
            f"The deployment {transaction_id} status hasn't changed to status {expected_status} after "
            f"{NUMBER_OF_RETRIES} retries that delayed {MILLISECONDS_BETWEEN_RETRIES}/1000 seconds. The actual status"
            f"of the deployment was: {transaction_response.status}")


class DeploymentStatusError(Exception):
    def __init__(self,
                 message: str):
        super().__init__(message)


class DeploymentNotValidError(Exception):
    def __init__(self,
                 message: str):
        super().__init__(message)
