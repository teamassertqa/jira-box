from jira_api.models.project import Project
from tests.e2e.infra.box_api.box_rest_client import BoxAPIClient
from tests.e2e.utils.deployment_sampler import DeploymentSampler


class DeploymentHelper:
    def __init__(self,
                 box_api: BoxAPIClient,
                 deployment_sampler: DeploymentSampler):
        self.box_api = box_api
        self.deployment_sampler = deployment_sampler

    def create_project(self,
                       project: Project,
                       expected_status: str = "SUCCEEDED") -> Project:
        deployment = self.box_api.project.create(project)

        deployment = self.deployment_sampler.validate_deployment_status(deployment.transaction_id, expected_status)

        return self.box_api.project.read(deployment.box_id)

    def delete_project(self,
                       project: Project,
                       expected_status: str = "SUCCEEDED"):
        deployment = self.box_api.project.delete(project.america_uuid)

        return self.deployment_sampler.validate_deployment_status(deployment.transaction_id, expected_status)

    def update_project(self,
                       deployment_id: int,
                       project: Project,
                       expected_status: str = "SUCCEEDED"):
        deployment = self.box_api.project.update(deployment_id, project)

        deployment = self.deployment_sampler.validate_deployment_status(deployment.transaction_id, expected_status)

        return self.box_api.project.read(deployment.box_id)
