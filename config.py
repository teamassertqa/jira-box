import os


class BaseConfig:
    DB_NAME = ''
    BOX_API = ''
    JIRA_URL = ''
    WORKFLOW_SCHEME_ID = None
    DB_SERVER_NAME = ''
    AD_SERVER_LIST = [
        "zzldap_server_1zz",
        "zzldap_server_2zz",
        "zzldap_server_3zz",
        "zzldap_server_4zz",
    ]
    LDAP_USERNAME = os.environ['LDAP_USERNAME']
    LDAP_PASSWORD = os.environ['LDAP_PASSWORD']
    ATLASSIAN_USERNAME = os.environ['ATLASSIAN_USERNAME']
    ATLASSIAN_PASSWORD = os.environ['ATLASSIAN_PASSWORD']
    AMERICA_SQLALCHEMY_URI = ''


class TestConfig(BaseConfig):
    CONFIG_NAME = 'TEST'
    WORKFLOW_SCHEME_ID = 13701
    DB_NAME = 'zzdb_test_namezz'
    DB_SERVER_NAME = 'zzdb_serverzz'
    JIRA_URL = 'zzjira_urlzz'
    BOX_API = 'zzbox_test_urlzz'
    AMERICA_SQLALCHEMY_URI = f'mssql+pymssql://{BaseConfig.ATLASSIAN_USERNAME}:{BaseConfig.ATLASSIAN_PASSWORD}@{DB_SERVER_NAME}/{DB_NAME}'


class DevConfig(BaseConfig):
    CONFIG_NAME = 'DEV'
    WORKFLOW_SCHEME_ID = 13701
    DB_NAME = 'zzdb_dev_namezz'
    DB_SERVER_NAME = 'zzdb_serverzz'
    JIRA_URL = TestConfig.JIRA_URL
    BOX_API = 'zzbox_urlzz'
    AMERICA_SQLALCHEMY_URI = f'mssql+pymssql://{BaseConfig.ATLASSIAN_USERNAME}:{BaseConfig.ATLASSIAN_PASSWORD}@{DB_SERVER_NAME}/{DB_NAME}'


class ProdConfig(BaseConfig):
    CONFIG_NAME = 'PROD'
    WORKFLOW_SCHEME_ID = 13701
    DB_NAME = 'zzdb_prod_namezz'
    DB_SERVER_NAME = 'zzdb_server_namezz'
    JIRA_URL = 'zzjira_urlzz'
    BOX_API = 'zzbox_urlzz'
    AMERICA_SQLALCHEMY_URI = f'mssql+pymssql://{BaseConfig.ATLASSIAN_USERNAME}:{BaseConfig.ATLASSIAN_PASSWORD}@{DB_SERVER_NAME}/{DB_NAME}'


class LocalConfig(DevConfig):
    CONFIG_NAME = 'LOCAL'
    BOX_API = 'zzbox_urlzz'

CONFIG = {
    'TEST': TestConfig,
    'DEFAULT': TestConfig,
    'DEVELOPMENT': DevConfig,
    'PRODUCTION': ProdConfig,
    'LOCAL': LocalConfig
}


def get_configuration():
    environment = os.getenv('FLASK_CONFIGURATION', 'DEFAULT')
    return CONFIG[environment]
