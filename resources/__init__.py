from jira_api.jira_api_client import JiraAPIClient

import config

configuration = config.get_configuration()

jira = JiraAPIClient(url=configuration.JIRA_URL,
                     username=configuration.ATLASSIAN_USERNAME,
                     password=configuration.ATLASSIAN_PASSWORD)
