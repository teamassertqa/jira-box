from flask import Blueprint
from flask_america import current_deployment, AmericaOperation, abort, find_deployment
from flask import request

import config
from factory import america_box
from jira_api.consts.project_roles import ProjectRoles
from jira_api.consts.routes import *
from jira_api.consts.swimlanes import SwimLanes
from jira_api.consts.working_days import WorkingDays
from jira_api.errors.jira_error import JiraError
from resources import jira, utils
from jira_api.models.project import Project

project_bp = Blueprint('project', __name__, url_prefix='/project')
configuration = config.get_configuration()


@project_bp.route('', methods=['POST'])
def create():
    return america_box.accept(operation_func=create_project_op,
                              operation_type=AmericaOperation.CREATE)


def create_project_op():
    current_deployment.name = request.json['name']
    current_deployment.properties = request.json['properties']
    current_deployment.properties['users'] = request.json['users']
    current_deployment.properties['user'] = request.json['user']

    project = Project.from_america(data=current_deployment.__dict__)

    try:
        if jira.project.is_exists(project.key):
            abort(error_title="Create error",
                  error_message=f"Project with key {project.key} already exists")

        project = jira.project.create(project=project)
        current_deployment.properties['id'] = project.jira_id

        perms_users = []
        perms_groups = []
        for member in project.users:
            if utils.ldap_member_exists(member):
                if utils.ldap_member_is_group(member):
                    perms_groups.append(utils.ldap_member_get_display_name(member))
                else:
                    perms_users.append(member)

        jira.project.add_roles(project.key, ProjectRoles.administrators.value, [project.user])
        jira.project.add_roles(project.key, ProjectRoles.developers.value, perms_users, perms_groups)

        # TODO: what we gonna do if that project was exist(and removed), but his boards left?
        boards = jira.board.get_project_boards(project.key)
        board_id = boards[0].id

        jira.project.setup_columns(board_id)

        current_deployment.details['board_id'] = board_id

        board_configuration = jira.board.get_board_configuration(board_id)

        filter = jira.filter.read(board_configuration['filter']['id'])
        filter.jql = f'project = {project.key} AND type != Test ORDER BY Rank ASC'
        jira.filter.update(board_configuration['filter']['id'], filter)

        jira.board.set_swimlane_strategy(id=board_id, strategy=SwimLanes.epics)

        jira.board.set_working_days(id=board_id, working_days=WorkingDays.day)

        added_devs = utils.add_quick_filters(board_id, project.users)
        current_deployment.details['quick_filter'] = added_devs

        current_deployment.save()
        print("Creating done")
    except JiraError as ex:
        abort(error_title="Create error",
              error_message=str(ex))


@project_bp.route('/<string:deployment_id>', methods=['GET'])
def read(deployment_id: str):
    deployment = find_deployment(deployment_id)
    project_id = deployment.properties['id']

    try:
        current_project = jira.project.read(project_id)
        america_project = Project.from_america(data=deployment.__dict__)

        if not current_project == america_project:
            deployment.name = current_project.name
            deployment.properties['name'] = current_project.name
            deployment.properties['key'] = current_project.key
            deployment.properties['lead'] = current_project.user
            deployment.properties['projectTypeKey'] = current_project.type_key
            deployment.properties['description'] = current_project.description
            deployment.properties['assigneeType'] = current_project.assignee_type
            deployment.properties['categoryId'] = current_project.category_id
            deployment.save()

        project_key = deployment.properties['key']
        external_urls = {
            'Jira URL': f'{configuration.JIRA_URL}/projects/{project_key}'
        }

        return deployment.to_response(urls=external_urls)
    except JiraError as ex:
        if str(ex).__contains__("No project could be found"):
            deployment.is_deleted = True
            deployment.save()
        abort(error_title="Read error",
              error_message=str(ex))


@project_bp.route('/<string:deployment_id>', methods=['PUT'])
def update(deployment_id: str):
    return america_box.accept(operation_func=update_project_op,
                              operation_type=AmericaOperation.UPDATE,
                              deployment_id=deployment_id)


def update_project_op():
    project_id = current_deployment.properties['id']
    current_deployment.properties = request.json['properties']

    current_deployment.name = request.json['name']
    current_deployment.properties['users'] = request.json['users']
    current_deployment.properties['user'] = request.json['user']
    current_deployment.properties['id'] = project_id

    new_project = Project.from_america(data=current_deployment.__dict__)

    try:
        jira.project.update(id=project_id, project=new_project)

        request_developers = utils.generate_users_list(request.json['users'])
        request_developers_display_names = []
        for user in request_developers:
            request_developers_display_names.append(user['username'])

        current_quick_filter = current_deployment.details['quick_filter']

        users_to_remove = list(set(current_quick_filter) - set(request_developers_display_names))
        users_to_add = list(set(request_developers_display_names) - set(current_quick_filter))

        if len(users_to_remove) != 0:
            utils.remove_quick_filters(current_deployment.details['board_id'], users_to_remove)

        if len(users_to_add) != 0:
            utils.add_quick_filters(current_deployment.details['board_id'], users_to_add)

        current_deployment.name = request.json['name']
        current_deployment.save()
    except JiraError as ex:
        if str(ex).__contains__("No project could be found"):
            current_deployment.is_deleted = True
            current_deployment.save()
        abort(error_title="Update error",
              error_message=str(ex))


@project_bp.route('/<string:deployment_id>', methods=['DELETE'])
def delete(deployment_id: str):
    return america_box.accept(operation_func=delete_project_op,
                              operation_type=AmericaOperation.DELETE,
                              deployment_id=deployment_id)


def delete_project_op():
    if not current_deployment.is_deleted:
        project_id = current_deployment.properties['id']
        key = current_deployment.properties['key']

        try:
            boards = jira.board.get_project_boards(project_id)
            for board in boards:
                jira.project.jira_conn.delete(path=f'{GREEN_HOPPER_ROUTE}/rapidview/{board.id}')

            jira.project.delete(id=project_id)

            current_deployment.is_deleted = True
            current_deployment.save()
        except JiraError as ex:
            if not jira.project.is_exists(key):
                current_deployment.is_deleted = True
                current_deployment.save()

            abort(error_title="Delete error",
                  error_message=str(ex))
