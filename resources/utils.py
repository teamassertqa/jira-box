import re

from flask import Blueprint, jsonify

from resources import jira
from jira_api.handlers import ldap_handler

project_bp = Blueprint('utils', __name__, url_prefix='/utils')


@project_bp.route('/category', methods=['GET'])
def get_category():
    data = jira.project.get_projects_categories()
    response = {}

    for project in data:
        response[project['name']] = project['id']

    return jsonify(response)


@project_bp.route('/keyvalidation/<string:key>', methods=['GET'])
def key_validation(key: str):
    data = jira.project.validate_key(key)
    response = {
        "validated": data.get('validated'),
        "message": data.get('error', '')
    }

    return jsonify(response)


@project_bp.route('/namevalidation/<string:name>', methods=['GET'])
def name_validation(name: str):
    projects = jira.project.get_all_projects()

    data = not any(x for x in projects if x.name == name)
    response = {
        "validated": data,
        "message": "Name used"
    }

    return jsonify(response)


@project_bp.route('/status', methods=['GET'])
def jira_status():
    return jsonify(jira.jira_status())


def add_quick_filters(board_id: int, users: list):
    if users is None:
        return []

    added_users = []
    developers = generate_users_list(users)

    for user in developers:
        display_name = user['displayName']
        if re.search(r'(?<=/.).*$', display_name):
            display_name = re.findall(r'(?<=/.).*$', display_name)[0]
            if re.search(r'(?<=\-.).*$', display_name):
                display_name = re.findall(r'(?<=\-.).*$', display_name)[0]

        added_users.append(user['username'])
        jira.board.add_quick_filter(board_id, display_name, user['username'])

    return added_users


def remove_quick_filters(board_id: int, users: list):
    if users is None:
        return

    filters = jira.board.get_quick_filters(board_id)

    for user in users:
        result = next(item for item in filters if item['query'] == f'assignee = {user}')
        jira.board.remove_quick_filter(board_id, result.get('id'))


def generate_users_list(users_list: list) -> list:
    users = []

    for user in users_list:
        ldap = ldap_handler.LDAPHandler()
        users += ldap.search_users(term=user, recursive=True)

    return list({v['username']: v for v in users}.values())


def ldap_member_exists(name: str) -> bool:
    ldap = ldap_handler.LDAPHandler()
    result = ldap.search_users(name)
    if len(result) > 0:
        return True
    return False


def ldap_member_is_group(name: str) -> bool:
    ldap = ldap_handler.LDAPHandler()
    result = ldap.search_users(name)
    if len(result) > 0 and result[0].get('isGroup') is True:
        return True
    return False


def ldap_member_get_display_name(name: str) -> bool:
    ldap = ldap_handler.LDAPHandler()
    return ldap.search_users(name)[0]['displayName']
